%global gohomepath github.com/containers
%global goipath %{gohomepath}/gvisor-tap-vsock

Summary:         Go replacement for libslirp and VPNKit
Name:            gvisor-tap-vsock
Version:         0.7.2
Release:         2%{?dist}
License:         Apache-2.0 AND BSD-2-Clause AND BSD-3-Clause AND MIT
URL:             https://%{gohomepath}/%{name}
Source0:         %{url}/archive/refs/tags/v%{version}.tar.gz
Patch3000:	 gvisor-tap-vsock-0.7.2-add-loong64-support.patch

BuildRequires: gcc glib2-devel glibc-devel glibc-static make
BuildRequires: golang git-core go-rpm-macros
Provides: podman-gvproxy = %{version}-%{release}
 
%description
A replacement for libslirp and VPNKit, written in pure Go.
It is based on the network stack of gVisor. Compared to libslirp,
gvisor-tap-vsock brings a configurable DNS server and
dynamic port forwarding.
 
%prep
%setup -n %{name}-%{version}
%ifarch loongarch64
export GOPROXY="https://goproxy.cn"
go get -d golang.org/x/sys@v0.19.0
go mod tidy
go mod download
go mod vendor
%patch 3000 -p1
%endif
%build

export GOPATH=$PWD/.gopath
mkdir -p ${GOPATH}/src/%{gohomepath}
ln -sfn $PWD $GOPATH/src/%{goipath}

%gobuild -o bin/gvproxy %{goipath}/cmd/gvproxy
%gobuild -o bin/gvforwarder %{goipath}/cmd/vm
 
%install
install -dp %{buildroot}%{_libexecdir}/podman
install -p -m0755 bin/gvproxy %{buildroot}%{_libexecdir}/podman
install -p -m0755 bin/gvforwarder %{buildroot}%{_libexecdir}/podman
 
%files
%license LICENSE
%doc README.md
%dir %{_libexecdir}/podman
%{_libexecdir}/podman/gvproxy
%{_libexecdir}/podman/gvforwarder
 
%changelog
* Wed May 08 2024 Huang Yang <huangyang@loongson.cn> - 0.7.2-2
- fix build error on loongarch64

* Wed Mar 13 2024 jackeyji <jackeyji@tencent.com> - 0.7.2-1
- initial build
